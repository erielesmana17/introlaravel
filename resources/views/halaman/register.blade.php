@extends('layouts.master')

@section('judul')
<h2>Buat Account Baru</h2>
<h3>Sign Up Form</h3>
@endsection

@section('content')

    <form action="/welcome" method="post">
    @csrf
    
<div>
    <div>
    <p><label for="fname">First Name :</label></p>
    <input type="text" name="fname" required/><br>
    </div>

    <div>
    <p><label for="lname">Last Name :</label></p>
    <input type="text" name="lname" required/>
    </div>
</div>

<div>
    <p><label for="Gender">Gender</label></p>
    <div>
    <input type="radio" id="male" name="Gender" required/>
    <label for="male">Male</label>
    </div>

    <div>
    <input type="radio" id="female" name="Gender" required/>
    <label for="female">Female</label>
    </div>
</div>
<br>
<div>
    <p><label for="nationality">Nationality</label></p>
    <select class="form-control" name="nationality">
        <option value="indo">Indonesia</option>
        <option value="amer">Amerika</option>
        <option value="inggris">Inggris</option>
    </select>
</div>    
    <div>
    <p><label for="language">Language Spoken</label></p>
    </div>
        
    <div>
    <input type="checkbox" id="bindo" name="language">
    <label for="bindo">Bahasa Indonesia</label><br>
    
    <div>
    <input type="checkbox" id="eng" name="language">
    <label for="eng">English</label><br>
    </div>
    
    <div>
    <input type="checkbox" id="oth" name="language">
    <label for="oth">Other</label><br>
    </div>
</div>
<br>
<div>
    <P>Bio</P>
    <textarea name="message" rows="10" cols="30"></textarea>
</div>
<input type="submit" value="Sign Up"></form></body>
</form>
@endsection